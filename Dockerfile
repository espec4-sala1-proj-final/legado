FROM openjdk:8-jre-alpine

WORKDIR /appagent

COPY target/legado-*.jar legado.jar

CMD ["java", "-jar", "legado.jar"]
