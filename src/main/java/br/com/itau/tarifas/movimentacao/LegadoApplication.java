package br.com.itau.tarifas.movimentacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LegadoApplication {

	public static void main(String[] args) {
		SpringApplication.run(LegadoApplication.class, args);
	}

}
