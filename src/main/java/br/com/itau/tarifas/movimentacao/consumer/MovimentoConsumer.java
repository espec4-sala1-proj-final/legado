package br.com.itau.tarifas.movimentacao.consumer;
import br.com.itau.tarifas.movimentacao.models.Movimentacao;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class MovimentoConsumer {

    @KafkaListener(topics = "spec4-gp1-movimento-tarifa", groupId = "SISTEMA-BT")
    public void receber(@Payload Movimentacao movimento) {
        System.out.println("Recebi o movimento TARIFA "+movimento.getIdTarifa()+" CNPJ/CPF: " + movimento.getCpfCnpj() + " VALOR R$: "+ movimento.getValorMovimento() + " ISENTO: "+ movimento.getIsento() + " TIPO: "+ movimento.getTipo()+ " DATA: "+ movimento.getDataMovimento());
    }
}