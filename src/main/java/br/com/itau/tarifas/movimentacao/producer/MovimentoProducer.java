package br.com.itau.tarifas.movimentacao.producer;

import br.com.itau.tarifas.movimentacao.models.Movimentacao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDate;

@Service
public class MovimentoProducer {
    @Autowired
    private KafkaTemplate<String, Movimentacao> movimentoProducer;

    public static BigDecimal multiplicar(BigDecimal primeiroNumero, int segundoNumero){
        return new BigDecimal(String.valueOf(primeiroNumero)).multiply(new BigDecimal(segundoNumero));
    }
    @PostConstruct
    public void registrarLogKafka() {
        BigDecimal valor = BigDecimal.valueOf(1.5);
        for(int i=1; i<11; i++) {
            Movimentacao movimento = new Movimentacao();
            movimento.setIdTarifa(1);
            movimento.setCpfCnpj("43776517000180");
            movimento.setIsento(false);
            movimento.setTipo("C");
            movimento.setDataMovimento(LocalDate.parse("2020-09-10"));
            valor = multiplicar(valor , 1);
            movimento.setValorMovimento(valor);
            System.out.println("Enviei o movimento TARIFA "+movimento.getIdTarifa()+" CNPJ/CPF: " + movimento.getCpfCnpj() + " VALOR R$: "+ movimento.getValorMovimento() + " ISENTO: "+ movimento.getIsento() + " TIPO: "+ movimento.getTipo()+ " DATA: "+ movimento.getDataMovimento());

            movimentoProducer.send("spec4-gp1-movimento-tarifa", movimento);
            movimento.setIdTarifa(7);
            movimento.setCpfCnpj("55194412002");
            movimento.setIsento(false);
            movimento.setTipo("C");
            movimento.setDataMovimento(LocalDate.parse("2020-09-11"));
            valor = multiplicar(valor , 1);
            movimento.setValorMovimento(valor);
            movimentoProducer.send("spec4-gp1-movimento-tarifa", movimento);
            System.out.println("Enviei o movimento TARIFA "+movimento.getIdTarifa()+" CNPJ/CPF: " + movimento.getCpfCnpj() + " VALOR R$: "+ movimento.getValorMovimento() + " ISENTO: "+ movimento.getIsento() + " TIPO: "+ movimento.getTipo()+ " DATA: "+ movimento.getDataMovimento());

        }
    }
}
